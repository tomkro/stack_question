package com.test.configuration;

import com.test.classes.TestClass;
import com.test.utilities.TestLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tkroth on 30/03/2016.
 */
@RestController
@Configuration
@ComponentScan(basePackages = {"com.test"})
public class AspectConfig {

    @Bean
    public TestLogger testLogger(){
        return new TestLogger();
    }

    @Autowired
    TestClass testClass;

    @RequestMapping("/")
    public int get() {
        return testClass.methodFromClass1();
    }

}
