package com.test.utilities;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

/**
 * Created by tkroth on 07/04/2016.
 */
@Aspect
public class TestLogger {

    public TestLogger() {
    }

    @Around("execution( * com.test.classes..*.*(..))")
    public Object aroundExecution(ProceedingJoinPoint pjp) throws Throwable {
        String packageName = pjp.getSignature().getDeclaringTypeName();
        String methodName = pjp.getSignature().getName();
        long start = System.currentTimeMillis();

        System.out.println(packageName + "." + methodName + " - Starting execution ");

        Object output = pjp.proceed();
        Long elapsedTime = System.currentTimeMillis() - start;

        System.out.println(packageName + "." + methodName + " - Ending execution ["+elapsedTime+" ms]");
        return output;
    }

}