package com.test.classes;

/**
 * Created by tkroth on 07/04/2016.
 */

public class TestClass2 {
    public int methodFromClass2() {
        int a = 10;
        int b = 5;

        return b + a;
    }
}
