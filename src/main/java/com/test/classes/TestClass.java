package com.test.classes;

import org.springframework.stereotype.Component;

/**
 * Created by tkroth on 07/04/2016.
 */
@Component
public class TestClass {

    public int methodFromClass1() {
        TestClass2 test = new TestClass2();
        return test.methodFromClass2();
    }

}
